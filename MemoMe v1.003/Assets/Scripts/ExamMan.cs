﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;						// For parsing string to int
using System.Linq;					// For displaying content of a list

public class ExamMan : MonoBehaviour {

	public GameObject Canvas;				// Referenced in Editor
	public GameObject HomeState;			// Referenced in Editor, for grabbing of article title
	private string ArticleTitle;
	public GameObject TitleDisplay;			// Referenced in Editor
	public GameObject ContentInputField;	// Referenced in Editor
	private string ArticleContent;			// For easier handling, instead of always getcomponent
	public GameObject ContentHintBuffer;	// Referenced in Editor, to help determine how many lines per page

	private int ContentLineCount;			// for easier handling, instead of always GetComponent<Text>().cachedTextGenerator.lineCount		
	private int DisplayedCharCount;			// for easier handling, instead of always GetComponent<Text> ().cachedTextGenerator.characterCountVisible
	public List <string> ContentInPages;	// For processing of Pages
	private List <int> PgCharNumber;		// For processing of Pages
	private int RemainingChars;				// For processing of Pages
	private int CurrentPage;				// To keep track of current page number
	public List <string> HintInPages;		// Copy of ContentInPages, but after processed to replace alphabets with '_'
	public List <string> AnswerInPages;	// Keep track of answer input by user (for changing page, score calc & review paper)

	public GameObject PageNumDisplay;		// Referenced in Editor (Page number display)
	public GameObject TotalPagesDisplay;	// Referenced in Editor (for "out of xx")
	public GameObject PrevPageButton;		// Referenced in Editor (for control enable/disable)
	public GameObject NextPageButton;		// Regerenced in Editor (for control enable/disable)
	private Color DefaultBlackTextColor;		// Holds original Color for text as we'll be manipulating Alpha
	private Color DefaultFadedBlackTextColor;	// Manipulate from PrevPageTextColor with Alpha of 0.3

	public GameObject PageNav;				// Referenced in Editor
	private int DifficultyLevel;			// to reference from MainEngine
	public GameObject DifficultyDisplay;	// Referenced in Editor

	public GameObject StudyState;			// Referenced in Editor (for start check)
	public GameObject PromptPanel;			// Referenced in Editor (Copy from Edit State)

	void OnEnable()
	{
		DifficultyLevel = (int)Canvas.GetComponent<MainEngine> ().DifficultySlider.GetComponent<Slider> ().value;
		DifficultyDisplay.GetComponent<Text> ().text = DifficultyLevel + "%";
		ArticleTitle = Canvas.GetComponent<MainEngine> ().ArticleTitle;

		bool ArticleVerified = false;
		if (ArticleTitle != "") {
			ArticleVerified = true;
		}

		if (HomeState.GetComponent<HomeMan> ().SelectedGO != null) {
			ArticleTitle = HomeState.GetComponent<HomeMan> ().SelectedGO.GetComponentInChildren<Text>().text;
			ArticleVerified = true;
		}

		if (ArticleVerified) {
			//Debug.Log ("Title: " + ArticleTitle);
//			if (ArticleTitle == ""){
//				ArticleTitle = 

			TitleDisplay.GetComponent<Text> ().text = ArticleTitle;
			ArticleContent = Canvas.GetComponent<MainEngine> ().LoadContentText (ArticleTitle);

			//ContentDisplay.GetComponent<Text> ().text = ArticleContent;
			ContentHintBuffer.GetComponent<Text> ().text = ArticleContent;
			RemainingChars = ArticleContent.Length;	
		}
			
		PgCharNumber = new List<int>();
		ContentInPages = new List<string> ();
		HintInPages = new List<string> ();
		AnswerInPages = new List<string> ();
		CurrentPage = 1;

		DefaultBlackTextColor = TitleDisplay.GetComponentInChildren<Text> ().color;
		DefaultFadedBlackTextColor = new Vector4 (DefaultBlackTextColor.r, DefaultBlackTextColor.g, DefaultBlackTextColor.b, DefaultBlackTextColor.a * 0.3f);
		PageNav.GetComponentInChildren<InputField> ().text = "";

		StartCoroutine ("AfterOneExamFrame");			// We need to get the number of lines only after text has been rendered after accomodating for text wrap etc...
	}

	void OnDisable(){
		//Debug.Log ("here");
		ArticleTitle = "";
		TitleDisplay.GetComponent<Text> ().text = "";
		ArticleContent = "";
		ContentHintBuffer.GetComponent<Text> ().text = "";
//		ContentInPages.Clear ();
//		HintInPages.Clear ();
		StopAllCoroutines ();
	}


	IEnumerator AfterOneExamFrame(){
		yield return new WaitForFixedUpdate ();
//		ContentLineCount = ContentBuffer.GetComponent<Text> ().cachedTextGenerator.lineCount;
		DisplayedCharCount = ContentHintBuffer.GetComponent<Text> ().cachedTextGenerator.characterCountVisible;
//		Debug.Log ("Number of Lines Displayed: " + DisplayedContentLineCount);

//		Debug.Log ("Remaining Chars#: " + RemainingChars + " , Displayed Char Count: " + DisplayedCharCount);
		while (RemainingChars > 0) {
			DisplayedCharCount = ContentHintBuffer.GetComponent<Text> ().cachedTextGenerator.characterCountVisible;
			PgCharNumber.Add (DisplayedCharCount);
			ContentInPages.Add(ContentHintBuffer.GetComponent<Text>().text.Substring(0,DisplayedCharCount));
			AnswerInPages.Add ("");		// Update the Array according to the number of pages
			// Debug.Log ("Page 1 Char count: " + DisplayedCharCount);
			RemainingChars -= DisplayedCharCount;
			ContentHintBuffer.GetComponent<Text> ().text = ContentHintBuffer.GetComponent<Text> ().text.Substring (DisplayedCharCount);
			UnityEngine.Canvas.ForceUpdateCanvases ();
//			Debug.Log ("Remaining Chars#: " + RemainingChars + " , Displayed Char Count: " + DisplayedCharCount);
		}

//		Debug.Log ("Total number of pages generated: " + PgCharNumber.Count);
//		int TotalCharChecker = 0;
//		for (int i = 0; i < PgCharNumber.Count; i++) {
//			Debug.Log ("Page " + i + ": " + PgCharNumber [i]);
//			TotalCharChecker += PgCharNumber [i];
//		}

		//Debug.Log ("Therefore total number of Chars from all pages: " + TotalCharChecker);
		//Debug.Log ("Versus original Article Text Length: " + ArticleContent.Length);

		//Debug.Log ("Content In Pages [0]: " + ContentInPages [0]);
		ProcessHint ();

		ContentInputField.GetComponent<InputField> ().text = AnswerInPages [0];


		ContentInputField.GetComponentInChildren<Text> ().text = HintInPages [0];
		PageNumDisplay.GetComponent<Text> ().text = "" + CurrentPage;
		TotalPagesDisplay.GetComponent<Text> ().text = "Out of " + ContentInPages.Count + ".";

		// Set button's activate/deactivate state
		UpdateButtonState();

	}

	public void GotoNextPage(){
		//Debug.Log ("Entered Go To Next Page()");
		if (CurrentPage < ContentInPages.Count) {

			// 1. Save the Answers from Current Page
			//Debug.Log ("Current Page: " + ContentInputField.transform.GetChild (2).GetComponent<Text> ().text);
			UpdateAnswer(ContentInputField.transform.GetChild (2).GetComponent<Text> ().text, CurrentPage-1);

			CurrentPage++;

			// 2. Load Answers from next page
			ContentInputField.GetComponentInChildren<InputField> ().text = AnswerInPages [CurrentPage - 1];

			// Will be updated with processed hint text instead
			ContentInputField.GetComponentInChildren<Text> ().text = HintInPages [CurrentPage-1];
			UnityEngine.Canvas.ForceUpdateCanvases ();
		}
		PageNumDisplay.GetComponent<Text> ().text = CurrentPage.ToString();
		UpdateButtonState ();

		//Debug.Log ("Current Page#: " + CurrentPage);
		//Debug.Log ("Line Count in Page: " + ContentDisplay.GetComponent<Text> ().cachedTextGenerator.characterCountVisible);
		//Debug.Log ("Number of Lines Visible: " + ContentDisplay.GetComponent<Text> ().cachedTextGenerator.lineCount);
	}

	public void GotoPreviousPage(){
		if (CurrentPage > 1) {

			// 1. Save the Answers from Current Page
			//Debug.Log ("Current Page: " + ContentInputField.transform.GetChild (2).GetComponent<Text> ().text);
			UpdateAnswer(ContentInputField.transform.GetChild (2).GetComponent<Text> ().text, CurrentPage-1);

			CurrentPage--;

			// 2. Load Answers from next page
			ContentInputField.GetComponentInChildren<InputField> ().text = AnswerInPages [CurrentPage - 1];

			// Will be updated with processed hint text instead
			ContentInputField.GetComponentInChildren<Text> ().text = HintInPages [CurrentPage-1];
			UnityEngine.Canvas.ForceUpdateCanvases ();
		}
		PageNumDisplay.GetComponent<Text> ().text = CurrentPage.ToString();
		UpdateButtonState ();
		//Debug.Log ("Current Page#: " + CurrentPage);
		//Debug.Log ("Line Count in Page: " + ContentDisplay.GetComponent<Text> ().cachedTextGenerator.characterCountVisible);
		//Debug.Log ("Number of Lines Visible: " + ContentDisplay.GetComponent<Text> ().cachedTextGenerator.lineCount);
	}
	private void UpdateButtonState(){
		// Check for Previous Pgae Button
		if (CurrentPage <= 1) {
			PrevPageButton.GetComponent<Button> ().interactable = false;
			PrevPageButton.GetComponentInChildren<Text> ().color = DefaultFadedBlackTextColor;
		} else {
			PrevPageButton.GetComponent<Button> ().interactable = true;
			PrevPageButton.GetComponentInChildren<Text> ().color = DefaultBlackTextColor;
		}

		// Check for Next Page Button
		if (CurrentPage >= ContentInPages.Count) {
			NextPageButton.GetComponent<Button> ().interactable = false;
			NextPageButton.GetComponentInChildren<Text> ().color = DefaultFadedBlackTextColor;
		} else {
			NextPageButton.GetComponent<Button> ().interactable = true;
			NextPageButton.GetComponentInChildren<Text> ().color = DefaultBlackTextColor;
		}

		// Check for Page Navigator Panel
		if (ContentInPages.Count == 1) {
			/* Disable Page Navigator
			 * 1. Grey out "Page:" text
			 * 2. Disable InputField
			 * 3. Disable "GO" Button */
			PageNav.transform.GetChild (0).GetComponentInChildren<Text> ().color = DefaultFadedBlackTextColor;	// "Page:"
			PageNav.GetComponentInChildren<InputField> ().interactable = false;
			PageNav.GetComponentInChildren<Button> ().interactable = false;
			PageNav.transform.GetChild(2).GetComponentInChildren<Text>().color = DefaultFadedBlackTextColor;	// "GO" button
		} else {
			PageNav.transform.GetChild (0).GetComponentInChildren<Text> ().color = DefaultBlackTextColor;		// "Page:"
			PageNav.GetComponentInChildren<InputField> ().interactable = true;
			PageNav.GetComponentInChildren<Button> ().interactable = true;
			PageNav.transform.GetChild(2).GetComponentInChildren<Text>().color = DefaultBlackTextColor;			// "GO" button
		}
	}
	public void GoButton_Clicked(){
		int PgNumEntered = Int32.Parse(PageNav.GetComponentInChildren<InputField> ().text);
		//Debug.Log ("Page Number Entered: " + PgNumEntered);
		if (PgNumEntered <= 0 || PgNumEntered > ContentInPages.Count) {
			//Debug.Log ("Invalid Page number Input");
			Canvas.GetComponent<MainEngine> ().ActivateFadingPrompt ("Invalid Page Number!");
		} else {
			//Debug.Log ("Jumping to Page#: " + PgNumEntered);
			UpdateAnswer(ContentInputField.transform.GetChild (2).GetComponent<Text> ().text, CurrentPage-1);
			CurrentPage = PgNumEntered;
			ContentInputField.GetComponentInChildren<Text> ().text = HintInPages [CurrentPage-1];
			ContentInputField.GetComponentInChildren<InputField> ().text = AnswerInPages [CurrentPage - 1];
			UpdateButtonState ();
		}
	}

	public void HomeButton_Clicked(){
		//Debug.Log ("Home Button Clicked!!");
		/* Things to do:
		 * 1. Go back to home state
		 * 2. Wipe out Back button history*/
		Canvas.GetComponent<MainEngine> ().OldState = true;			// Disable updating of state history (no not save new state)
		Canvas.GetComponent<MainEngine> ().StateHistory.Clear ();	// Blast clear State History
		Canvas.GetComponent<MainEngine> ().UpdateGameState (1);		// 1 to home state
	}

	public void FinishedButton_Clicked(){
		//Debug.Log ("Finished Button clicked!!");
		UpdateAnswer(ContentInputField.transform.GetChild (2).GetComponent<Text> ().text, CurrentPage-1);
		int NewGameState = 6;
		Canvas.GetComponent<MainEngine> ().UpdateGameState (NewGameState);

	}

	public void ClearButton_Clicked(){

		string ContentText = ContentInputField.GetComponent<InputField>().text;	
		if (ContentText == "") {
			// Let user know content has been cleared.
			Canvas.GetComponent<MainEngine>().ActivateFadingPrompt ("Content Cleared!");		
		}
		else {
			PromptPanel.SetActive (true);	
		} 
	}

	public void Clicked_Prompt_NO(){
		PromptPanel.SetActive (false);
	}

	public void Clicked_Prompt_YES(){
		ContentInputField.GetComponent<InputField>().text = "";
		PromptPanel.SetActive (false);
		UpdateAnswer ("", CurrentPage - 1);
		Canvas.GetComponent<MainEngine>().ActivateFadingPrompt ("Content Cleared!");
	}

	// So now all contents are stored in pages in an array of strings
	/* Need to:
	 * 1. Make a copy of the content
	 * 2. Change % of text in the content to '_'
	 * 3. Change the content hint to load from this modified text buffer */
	// ContentDisplay.GetComponent<Text> ().text = ContentInPages [0];

	public void ProcessHint(){
		// Change
		//HintInPages = new List<string> ();

		// Loop through all pages to proceess each page's hint
		for (int i = 0; i < ContentInPages.Count ; i++)
		{
			string ProcessingPage = ContentInPages [i];
			Debug.Log ("Count for HintInPages: " + ContentInPages.Count);
			Debug.Log ("Difficulty Level: " + DifficultyLevel);

			// ContentHintBuffer.GetComponent<Text> ().text = HintInPages[CurrentPage];
			/* Thigns to do:
			 * 1. get original hint in page
			 * 		ContentHintBuffer.GetComponent<Text> ().text = HintInPages[CurrentPage];
			 * 2. Inject the processing code
			 * 3. repeat the processing code for the number of pages available */

			/* --------------- Processing Codes ------------------ */
			// 1. Find the number of letters to blanko
			int CharCounts = ProcessingPage.Count (char.IsLetterOrDigit);	// Count only AlphaNeumeric characters
//			Debug.Log("N.O. of Alphaneumeric chars (CharCounts): " + CharCounts);

			int NumOfBlanko = CharCounts * DifficultyLevel / 100;

			// We must have at least 1 Blanko at minimum
			if (NumOfBlanko == 0) {
				NumOfBlanko = 1;
			}
		
			Debug.Log ("Difficulty Level: " + DifficultyLevel);
			Debug.Log ("Number of Letters to Blanko: " + NumOfBlanko);
			Debug.Log("N.O. of Alphaneumeric chars (CharCounts): " + CharCounts);

			// Note: Length Might return more than seen because of /n (newline)
			// But doesn't matter cause we'll check if the position is valid
			//Debug.Log ("Length of Article: " + HintInPages [0].length);
			/*for (int i = 0; i < HintInPages [0].Length; i++) {
				Debug.Log ("At position " + i + " : " + HintInPages [0] [i]);
			}*/	
			// *Found 3 empty white space at the end of a single line article

			// 2. Create and Array to store all Randomly Generated Blank Locations
			int[] BlankLocations = new int[NumOfBlanko];
			/* Can Skip initialization because C# always have default value
			 * for (int i = 0; i < BlankLocations.Length; i++) {
				// Initialize Array to all contents 0
				BlankLocations [i] = 0;
				} */

			//Debug.Log ("Processing Page's Length: " + ProcessingPage.Length);
			char[] ProcessingPageArr = ProcessingPage.ToCharArray ();
			// 3. Generate a Position Number for each Blank Location
			if (DifficultyLevel == 100)	{
				// when NumberOfBlanko = CharCounts, original check will stuck in inifite loop as position 0 is already in BlankLocations[]
				// i.e. BlankLocations[] has all elements of 0 by default
				// So we just loop through all chars and change them to _
				for (int j = 0; j < ProcessingPage.Length; j++){
					if (Char.IsLetterOrDigit(ProcessingPageArr[j])){
						ProcessingPageArr[j] = '_';
					}
				}
			} else {
				for (int j = 0; j< NumOfBlanko; j++){
					// 3A. Generate a Random Number for Blank Location
					int TempBlankLocation = UnityEngine.Random.Range(0, ProcessingPage.Length );
					// 3B.1. Check if the Blank Location is an AlphaNeumeric
					// 3B.2. Check if the Blank Location is already inside BlankLocations[]
					while (!Char.IsLetterOrDigit( ProcessingPage[TempBlankLocation]) || BlankLocations.Contains(TempBlankLocation))
					{
						// If position is not originally an ANMric or location already used previously,
						// regenerate another location
						TempBlankLocation = UnityEngine.Random.Range(0, ProcessingPage.Length );
					}

					// 4. Update BlankLocations[] of the valid Blank Location Generated;
					BlankLocations[j] = TempBlankLocation;
				}


				//char[] ProcessingPageArr = ProcessingPage.ToCharArray ();
				// 5. Change all Chars at location from BlankLocations[] to '_'
				for (int j = 0; j < BlankLocations.Length; j++ )
				{
					ProcessingPageArr[BlankLocations[j]] = '_';
				}
			}
			//Debug.Log ("BlankLocations.length = " + BlankLocations.Length);
			//string result = string.Join (",", Array.ConvertAll (BlankLocations, x => x.ToString ()));
			//Debug.Log ("BlankLocations[] = (" + result + ")");


			//string result = string.Join (",", Array.ConvertAll (ProcessingPageArr, x => x.ToString ()));
			//Debug.Log ("ProcessingPageArr[] = (" + result + ")");

			// 6. Update Current displayed hint with the new processed hint
			string ProcessedHint = new string(ProcessingPageArr);
			//Debug.Log ("ProcessedHint.Length = " + ProcessedHint.Length);
			HintInPages.Add(ProcessedHint);

		}

	}

	public void UpdateAnswer(string CurrentAnswer, int CurPageNum){
		//Debug.Log ("UpdatedAnswer: " + CurrentAnswer);
		//Debug.Log ("Saved Page: " + CurPageNum);
		AnswerInPages [CurPageNum] = CurrentAnswer;
	}
}

