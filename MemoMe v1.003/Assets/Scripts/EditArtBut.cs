﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EditArtBut : MonoBehaviour {

	private GameObject HomeState;
	private GameObject Canvas;

	// Use this for initialization
	void Start () {
		//Debug.Log ("Yo~");
		HomeState = GameObject.Find("HomeState");
		Canvas = GameObject.Find ("Canvas");
		Button btn = this.GetComponent<Button>();
		btn.onClick.AddListener(EditButton_Clicked);
	}
	
	private void EditButton_Clicked(){
		HomeState.GetComponent<HomeMan> ().SelectedGO = this.gameObject.transform.parent.parent.gameObject;
		Canvas.GetComponent<MainEngine>().SelectedGO = this.gameObject.transform.parent.parent.gameObject;
		//Parse this GameObject to MainEngine...

		HomeState.GetComponent<HomeMan> ().EditButton_Clicked ();		// Bring control back to Home Manager
	}
}
