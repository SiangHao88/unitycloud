﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;					// For File operations
using System.Linq;					// For using myString.All() / Contains() function

public class EditMan : MonoBehaviour {

	public GameObject Canvas;				// Referenced in Editor
	public GameObject HomeState;			// Referenced in Editor (for Delete button clicked)
	private string ArticleTitle;
	public GameObject TitleInputField;		// Referenced in Editor
	public GameObject ContentInputField;	// Referenced in Editor
	private string ArticleContent;			// For easier handling, instead of always getcomponent
//	private string ArticleContentBuffer;	// Clone of ArticleContent, used for processing of pages
	public GameObject ContentBufferText;	// Referenced in Editor, to help determine how many lines per page
	private float OriginalContentHeight;	// For manipulating SizeDelta when content overflows out of ContentInputField

	private int ContentLineCount;			// for easier handling, instead of always GetComponent<Text>().cachedTextGenerator.lineCount		
	public GameObject PromptPanel;			// Referenced in Editor


	/*Things to do in EditMan:
	 * 1. Load Article Title (Done)
	 * 2. Load Content (Done)
	 * 3. Implement Scroll content / Scrollbar (Done)
	 * 3. Implement 16k char limit (Done)
	 * 3.1. prompt user to use the new page button (Done)
	 * 4. Hook Up Clear Button (Done)
	 * 5. Interface: Blank out those unused, except next page (Done)
	 * 6. Create String List to store content as pages
	 * 7. Update Page number on top
	 * 8. Set up page Navigation system
	 *... */

	void OnEnable()
	{
		//ArticleTitle = Canvas.GetComponent<MainEngine>();
		if (Canvas.GetComponent<MainEngine> ().SelectedGO != null) {
			ArticleTitle = Canvas.GetComponent<MainEngine> ().SelectedGO.GetComponentInChildren<Text>().text;
			TitleInputField.GetComponent<InputField> ().text = ArticleTitle;

			ArticleContent = Canvas.GetComponent<MainEngine> ().LoadContentText (ArticleTitle);
			OriginalContentHeight = ContentInputField.GetComponent<RectTransform> ().rect.height;
			ContentInputField.GetComponent<InputField> ().text = ArticleContent;
			ContentBufferText.GetComponent<Text> ().text = ArticleContent;

			StartCoroutine ("EditAfterOneFrame");			// Let the Buffer populate first, in order to get the right value
		}
	}

	IEnumerator EditAfterOneFrame(){
		yield return new WaitForFixedUpdate ();
		// Update the size of InputField so that scrollbar can reflect accordingly
		ResizeEditContent();
	}


	public void ResizeEditContent()
	{
		ContentBufferText.GetComponent<Text>().text = ContentInputField.GetComponent<InputField>().text;
		if (ContentBufferText.GetComponent<Text> ().text == "") {
			// Forces ContentSizeFitter to shrink to size of 1 line only
			ContentBufferText.GetComponent<Text> ().text = "A";
			ContentBufferText.GetComponent<Text> ().text = "";
		}

		if (ContentBufferText.GetComponent<RectTransform> ().rect.height > OriginalContentHeight) {
			ContentInputField.GetComponent<RectTransform> ().sizeDelta = new Vector2 (0, ContentBufferText.GetComponent<RectTransform> ().rect.height - OriginalContentHeight);
		} else if (ContentBufferText.GetComponent<RectTransform> ().rect.height < OriginalContentHeight){
			ContentInputField.GetComponent<RectTransform> ().sizeDelta = new Vector2 (0, 0);
		}

		if (ContentInputField.GetComponent<InputField>().text.Length >= 16000){
			Canvas.GetComponent<MainEngine> ().ActivateFadingPrompt ("Page Full!!\nPlease use 'Next Page'!!");
		}
	}

	public void ClearButton_Clicked(){
		
		string ContentText = ContentInputField.GetComponent<InputField>().text;	
		if (ContentText == "") {
			// Let user know content has been cleared.
			Canvas.GetComponent<MainEngine>().ActivateFadingPrompt ("Content Cleared!");		
		}
		else {
			PromptPanel.SetActive (true);	
		} 
	}
		
	public void Clicked_Prompt_NO(){
		PromptPanel.SetActive (false);
	}

	public void Clicked_Prompt_YES(){
		//ContentInputField.GetComponent<InputField>().text ="";
		CleanUpContentIF();
		PromptPanel.SetActive (false);
		Canvas.GetComponent<MainEngine>().ActivateFadingPrompt ("Content Cleared!");
	}
		
	public void Clicked_Save(){
		/* Things to do:
		 * 1. Check if Title is empty, prompt (Done)
		 * 2. Check if File Name contains invalid characters (Done)
		 * 3. Check if File Name Already exist (Needs modification)
		 * 4. Check if Content is empty, prompt (Done)
		 * 5. Save: write to file*/

		// Debug.Log ("Clicked_Save ArticlePath: "+ArticlePath);
		string FileName = TitleInputField.GetComponent<InputField> ().text;			// Convenience for reference below
		string FileContent = ContentInputField.GetComponent<InputField> ().text;	// Convenience for reference below
		string ArticlePath = Application.persistentDataPath + "/StudyMaterial";

		if (FileName == "") {
			Canvas.GetComponent<MainEngine>().ActivateFadingPrompt ("Please Input A Title!");
		} else if (File.Exists (ArticlePath + "/" + FileName + ".txt") && FileName != ArticleTitle) {
			Canvas.GetComponent<MainEngine>().ActivateFadingPrompt ("Title Already Exist! \nPlease Use A New Title");
		} else if (FileContent == "") {
			Canvas.GetComponent<MainEngine>().ActivateFadingPrompt ("Content Is Empty!");
		} else if (FileContent.Count (char.IsLetterOrDigit) < 1) {
			Canvas.GetComponent<MainEngine>().ActivateFadingPrompt ("Invalid Content!\nContent must have at least 1 Alphanumeric");
		} else if (FileName.IndexOfAny (Path.GetInvalidFileNameChars ()) >= 0) {
			/* The following characters are invalid in a filename:
			 * Char    Hex Value
			 * ",      0022
			 * <,      003C
			 * >,      003E
			 * |,      007C
			 * -----------------------------------------------------*/
			Canvas.GetComponent<MainEngine>().ActivateFadingPrompt ("Invalid Title!");
		} else if (FileName.All(char.IsWhiteSpace)) {
			// File Name Only contains White space
			Canvas.GetComponent<MainEngine>().ActivateFadingPrompt ("Please input a proper Title!");
		} else {
			// Save to file
			// Modification: Before saving, delete selected article
			// Need to Activate Home State first for that ArticleList and relevant variables to be alive
			Canvas.GetComponent<MainEngine>().UpdateGameState(1);
			// Debug.Log("Article Index: " + HomeState.GetComponent<HomeMan> ().ArticleIndex);
			HomeState.GetComponent<HomeMan> ().DeleteArticle (HomeState.GetComponent<HomeMan> ().ArticleIndex);
			Canvas.GetComponent<MainEngine>().SaveArticle(FileName, FileContent);
			// Debug.Log("ArticleList Count: " + HomeState.GetComponent<HomeMan> ().ArticleList.Count);
			TitleInputField.GetComponent<InputField> ().text = "";
			CleanUpContentIF();

			// Force Home State to refresh
			HomeState.SetActive (false);
			HomeState.SetActive (true);
		}
	}


	private void CleanUpContentIF(){
		ContentInputField.GetComponent<InputField>().text ="";
		ContentBufferText.GetComponent<Text> ().text = "";

		// sizeDelta ContentInputField 0,0 because it's the text inside that has a padding of -10 on both left and right
		// sizeDelta ContentBufferText -20 because it's -10 on both left and right
		ContentInputField.GetComponent<RectTransform> ().sizeDelta = new Vector2 (0,0);
		ContentBufferText.GetComponent<RectTransform> ().sizeDelta = new Vector2 (-20,0); 
		// CIF DeltaX -150 because -75 on both sides
		UnityEngine.Canvas.ForceUpdateCanvases ();
	}

	// Update is called once per frame
	void Update () {
		if (Input.anyKeyDown || Input.GetMouseButtonUp(0)) {
			ResizeEditContent();
		}
	}//
}
