﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;					// For File operations
using System.Linq;					// For using myString.All() / Contains() function

public class AddMan : MonoBehaviour {
	/* Things to do:
	 * 1. Upon Clicking Clear Button
	 * 1a. Check if content is empty ==> Show Prompt
	 * 1a.i ) If user Click Yes, clear ContentInputField
	 * 1a.ii) If user Click No, hide Show Prompt
	 * 1b. Hide Show Prompt (if not hidden)
	 * 2. Upon Saving check if 
	 * 2a. Heading is empty
	 * 2b. Heading already exist
	 * 2c. Head has any invalid characters
	 * 2d. Content is empty 
	 * 3. save file to text */

	public GameObject Canvas;					// Referenced in Editor
	public GameObject TitleInputField;			// Referenced in Editor
	public GameObject ContentInputField;		// Referenced in Editor
	public GameObject BufferContentIF;			// Referenced in Editor
	public GameObject ClearBut;					// Referenced in Editor
	public GameObject SaveBut;					// Referenced in Editor


	// Fading Prompt related variables
	public GameObject PromptPanel;				// Referenced from Editor

	// Save / File IO related Variables
	private string ArticlePath;

	private float ContentInputScrollPanelHeight;// Holds original Scroll Panel's Height to reset scrollbar during 

	// Use this for initialization
	void Start () {
		PromptPanel.SetActive (false);
		ArticlePath = Canvas.GetComponent<MainEngine> ().ArticlePath;

		// Debug.Log ("Start - Height of CIF: " + ContentInputField.GetComponent<RectTransform> ().rect.height);
	}

	public void ValueChangeCheck(){
		
	}

	public void Clicked_Clear(){
		string ContentText = ContentInputField.GetComponent<InputField>().text;	
		if (ContentText == "") {
			// Let user know content has been cleared.
			Canvas.GetComponent<MainEngine>().ActivateFadingPrompt ("Content Cleared!");		
		}
		else {
			PromptPanel.SetActive (true);	
		}
	}

	public void Clicked_Prompt_NO(){
		PromptPanel.SetActive (false);
	}

	public void Clicked_Prompt_YES(){
		//ContentInputField.GetComponent<InputField>().text ="";
		CleanUpContentIF();
		PromptPanel.SetActive (false);
		Canvas.GetComponent<MainEngine>().ActivateFadingPrompt ("Content Cleared!");
	}

	private void CleanUpContentIF(){
		ContentInputField.GetComponent<InputField>().text ="";
		BufferContentIF.GetComponent<Text> ().text = "";
		//ContentInputField.GetComponent<RectTransform> ().sizeDelta = new Vector2 (0,ContentInputScrollPanelHeight);
		//BufferContentIF.GetComponent<RectTransform> ().sizeDelta = new Vector2 (0,ContentInputScrollPanelHeight);
		ContentInputField.GetComponent<RectTransform> ().sizeDelta = new Vector2 (0,0);
		BufferContentIF.GetComponent<RectTransform> ().sizeDelta = new Vector2 (0,0);
		// CIF DeltaX -150 because -75 on both sides
		UnityEngine.Canvas.ForceUpdateCanvases ();
	}

	public void Clicked_Save(){
		/* Things to do:
		 * 1. Check if Title is empty, prompt
		 * 2. Check if Title is only white space, prompt
		 * 3. Check if Title/FileName contains invalid characters, prompt
		 * 4. Check if File Name Already exist, prompt
		 * 5. Check if content is empty, prompt
		 * 6. Check if Content holds any alphanumeric, prompt
		 * 7. Save: write to file*/

		// Debug.Log ("Clicked_Save ArticlePath: "+ArticlePath);
		string FileName = TitleInputField.GetComponent<InputField> ().text;			// Convenience for reference below
		string FileContent = ContentInputField.GetComponent<InputField> ().text;	// Convenience for reference below

		if (FileName == "") {
			Canvas.GetComponent<MainEngine>().ActivateFadingPrompt ("Please Input A Title!");
		} else if (FileName.All(char.IsWhiteSpace)) {
			// File Name Only contains White space
			Canvas.GetComponent<MainEngine>().ActivateFadingPrompt ("Please input a proper Title!");
		} else if (FileName.IndexOfAny (Path.GetInvalidFileNameChars ()) >= 0) {
			Canvas.GetComponent<MainEngine>().ActivateFadingPrompt ("Invalid Title!");
		} else if (File.Exists (ArticlePath + "/" + FileName + ".txt")) {
			Canvas.GetComponent<MainEngine>().ActivateFadingPrompt ("Title Already Exist! \nPlease Use A New Title");
		} else if (FileContent == "") {
			Canvas.GetComponent<MainEngine>().ActivateFadingPrompt ("Content Is Empty!");
		} else if (FileContent.Count (char.IsLetterOrDigit) < 1) {
			Canvas.GetComponent<MainEngine>().ActivateFadingPrompt ("Invalid Content!\nContent must have at least 1 Alphanumeric");
		} else {
			// Save to file
			Canvas.GetComponent<MainEngine>().SaveArticle(FileName, FileContent);
			TitleInputField.GetComponent<InputField> ().text = "";
			//ContentInputField.GetComponent<InputField> ().text = "";
			CleanUpContentIF();

			// Go back to previous state
			Canvas.GetComponent<MainEngine>().UpdateGameState(1);
		}
	}
		
	// Update is called once per frame
	void Update () {

	}
}
