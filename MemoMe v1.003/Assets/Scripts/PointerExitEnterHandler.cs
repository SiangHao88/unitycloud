﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;			// For Pointer Handlers
using UnityEngine.UI;					// for Image and Color

public class PointerExitEnterHandler : MonoBehaviour , IPointerEnterHandler, IPointerExitHandler{

	public bool MouseOver;
	public bool Choosen;
	public GameObject HomeState;
	private Vector4 PanelHighlitedColor;	// Change the Panel's Color if it is selected
	private Vector4 PanelUnHighlitedColor;	// Change the Panel's Color if it is selected
	private GameObject IArtPanel;

	void OnEnable(){
		MouseOver = false;
		Choosen = false;
		HomeState = GameObject.Find ("HomeState");
		PanelHighlitedColor = new Vector4(0,.91f,1,0.19f);
		PanelUnHighlitedColor = new Vector4(0,.91f,1,0);
		IArtPanel = this.gameObject;
	}

	public void OnPointerEnter(PointerEventData eventData){
		MouseOver = true;
		IArtPanel.GetComponent<Image> ().color = PanelHighlitedColor;
	}
	public void OnPointerExit(PointerEventData eventData){
		// Check if Choosen
		MouseOver = false;

		if (!Choosen) {
			IArtPanel.GetComponent<Image> ().color = PanelUnHighlitedColor;
			this.transform.GetChild (0).gameObject.GetComponent<ScrollSnapScript> ().PanelSelected = false;
		} else {
			IArtPanel.GetComponent<Image> ().color = PanelHighlitedColor;
			this.transform.GetChild (0).gameObject.GetComponent<ScrollSnapScript> ().PanelSelected = true;
		}
	}

	void Update()
	{
		if (Input.GetMouseButtonDown (0) ){
			if (MouseOver) {
				Choosen = true;
				HomeState.GetComponent<HomeMan> ().SelectedGO = this.gameObject;
				IArtPanel.GetComponent<Image> ().color = PanelHighlitedColor;
				this.transform.GetChild (0).gameObject.GetComponent<ScrollSnapScript> ().PanelSelected = true;
				//Parse Panel Selected to ScrollSnapScript
				//Debug.Log (this.transform.GetChild (0).GetChild(0).GetChild(0).GetComponent<Text>().text);
			} else {
				Choosen = false;
				IArtPanel.GetComponent<Image> ().color = PanelUnHighlitedColor;
				this.transform.GetChild (0).gameObject.GetComponent<ScrollSnapScript> ().PanelSelected = false;
			}
		}
			
	}
}
