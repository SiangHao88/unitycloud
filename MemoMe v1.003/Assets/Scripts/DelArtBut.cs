﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DelArtBut : MonoBehaviour {

	private GameObject HomeState;

	// We Can't add function to button click for a prefab in Editor, 
	// hence need to attach this script to add the function
	void Start () {
		//Debug.Log ("Yo~");
		HomeState = GameObject.Find("HomeState");
		Button btn = this.GetComponent<Button>();
		btn.onClick.AddListener(DelButton_Clicked);
	}

	private void DelButton_Clicked(){
		HomeState.GetComponent<HomeMan> ().SelectedGO = this.gameObject.transform.parent.parent.gameObject;
		HomeState.GetComponent<HomeMan> ().DelButton_Clicked ();		// Bring control back to Home Manager
	}
}
