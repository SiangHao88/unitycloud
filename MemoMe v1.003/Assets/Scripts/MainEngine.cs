﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class MainEngine : MonoBehaviour {

	// Public GObj updated in Editor
	public GameObject DifficultySlider;
	public GameObject DifficultyDisplay;
	public GameObject AddBut;

	public int GameState;					// Game State number controller 1 to 7
	public List<int> StateHistory;			// To Keep track of states while using the back button
	public bool OldState;					// Control: If Back button is pressed, DO NOT add new state
	public GameObject HomeState;			// Get a reference on Home Manager from
	public GameObject AddState;				// Get a reference on Add Manager
	public GameObject EditState;			// Get a reference on Edit Manager
	public GameObject StudyState;			// Get a reference of Study Manager
	public GameObject ExamState;			// Get a reference on Exam Manger
	public GameObject ResultState;			// Get a reference from 

	public List<GameObject> ArticleList;	// List of all the articles
	public GameObject SelectedGO;			// Get a holder of Selected IArtPanel. Set by HomeMan, referenced by all others
	public string ArticlePath;
	public string ArticleTitle;				// For all other states to check if a valid article has been selected

	// Fading Prompt related variables
	public float FadeDuration;
	public float FadeSpeed;
	public float TimeActivatedFade;
	public bool ShowFadingPrompt;
	public Color OriginalFPanelColor;
	public Color OriginalFTextColor;
	public GameObject FadingPromptPanel;		// Referenced in Editor

	public GameObject BackButton;			// Referenced in Editor

	// Use this for initialization
	void Start () {
		// Initialize Variables
		ArticleList = new List<GameObject>();
		// No need to instantiate GameObjects as C# has default values
		ArticlePath = Application.persistentDataPath + "/StudyMaterial";
		// Folder At: C:/Users/siang/AppData/LocalLow/DefaultCompany/MemoMe/StudyMaterial

		StateHistory = new List<int> ();
		OldState = false;

		// Fading Prompt related variables
		FadingPromptPanel.SetActive (false);
		FadeDuration = 3;
		FadeSpeed = 0;
		TimeActivatedFade = 0;
		ShowFadingPrompt = false;
		OriginalFPanelColor = new Vector4 (0, 0, 0, 0);
		OriginalFTextColor = new Vector4 (0, 0, 0, 0);
		ArticleTitle = "";

		GameState = 1;						
		UpdateGameState (1);				// The App Starts off with State 1: Home
	}

	// Handlers on Android physical Menu and Back Button
	void OnGUI() {
		var e = Event.current;
		switch (e.type) {
		case EventType.KeyUp:
			switch (e.keyCode) {
			case KeyCode.Menu:
				Debug.Log ("Menu Button is pressed");
				e.Use ();
				break;
			case KeyCode.Escape:
				Debug.Log ("Android Back Button is pressed");
				if (GameState != 5 || GameState != 6) {			// Back Button Disabled during Exam and Result
					GoToPreviousState ();
				}
				e.Use ();
				break;
			}
			break;
		}
	}

	public void UpdateGameState(int _GameState) {
		/* GameStates:
		 * 1. Home / Main Menu
		 * 2. Add New Article
		 * 3. Study
		 * 4. Edit Selected Article
		 * 5. Exam
		 * 6. Result
		 * 7. Review	*/

		GameState = _GameState;
		if (GameState == 1) {				// GameState 1: Home / MainMenu
			HomeState.SetActive (true);
			AddState.SetActive (false);
			StudyState.SetActive (false);
			EditState.SetActive (false);
			ExamState.SetActive (false);
			ResultState.SetActive (false);
			BackButton.SetActive (true);
		} else if (GameState == 2) {		// GameState 2: Add New Article
			HomeState.SetActive (false);
			AddState.SetActive (true);
			StudyState.SetActive (false);
			EditState.SetActive (false);
			ExamState.SetActive (false);
			ResultState.SetActive (false);
			BackButton.SetActive (true);
		} else if (GameState == 3) {		// GameState 3: Study
			HomeState.SetActive (false);
			AddState.SetActive (false);
			StudyState.SetActive (true);
			EditState.SetActive (false);
			ExamState.SetActive (false);
			ResultState.SetActive (false);
			BackButton.SetActive (true);
		} else if (GameState == 4) {		// GameState 4: Edit Selected Article
			HomeState.SetActive (false);
			AddState.SetActive (false);
			StudyState.SetActive (false);
			EditState.SetActive (true);
			ExamState.SetActive (false);
			ResultState.SetActive (false);
			BackButton.SetActive (true);
		} else if (GameState == 5) {		// GameState 5: Exam 
			HomeState.SetActive (false);
			AddState.SetActive (false);
			StudyState.SetActive (false);
			EditState.SetActive (false);
			ExamState.SetActive (true);
			ResultState.SetActive (false);
			BackButton.SetActive (false);
		} else if (GameState == 6) {		// GameState 5: Result
			HomeState.SetActive (false);
			AddState.SetActive (false);
			StudyState.SetActive (false);
			EditState.SetActive (false);
			ExamState.SetActive (false);
			ResultState.SetActive (true);
			BackButton.SetActive (false);
		}

		if (!OldState) {
			StateHistory.Insert (0, GameState);
		}
		OldState = false;
		// CheckHistory ();
	}

	public void GoToPreviousState(){
		//Debug.Log ("entered GoToPreviousState()");
		if (StateHistory.Count > 1) {
			StateHistory.RemoveAt (0);
			OldState = true;
			UpdateGameState (StateHistory [0]);
		}
	}

	private void CheckHistory(){
		string checker = "Contents of StateHistory: ";
		foreach (int i in StateHistory) {
			checker += i.ToString () + " , ";
		}
		Debug.Log (checker);
	}
		
	// Prompts user that Content is already cleared
	public void ActivateFadingPrompt(string PromptText)
	{
		FadingPromptPanel.GetComponentInChildren<Text> ().text = PromptText;
		FadingPromptPanel.SetActive (true);
		TimeActivatedFade = Time.time;
		ShowFadingPrompt = true;			// Triggers fading code in Update
	}

	public string LoadContentText(string ArticleTitle){
		string tempContent = File.ReadAllText(ArticlePath + "/" + ArticleTitle + ".txt");
		return tempContent;
	}

	public void SaveArticle(string FileName, string FileContent){
		var sr = File.CreateText (ArticlePath + "/"+ FileName + ".txt");
		sr.WriteLine (FileContent);
		sr.Close ();
		ActivateFadingPrompt ("Article Successfully Saved!");
		UnityEngine.Canvas.ForceUpdateCanvases ();
	}

	// Update is called once per frame
	void Update () {
		// Fading Prompt Animation
		if (ShowFadingPrompt) {
			if (Time.time - TimeActivatedFade <= FadeDuration) {
				OriginalFPanelColor = FadingPromptPanel.GetComponent<Image> ().color;
				OriginalFTextColor = FadingPromptPanel.GetComponentInChildren<Text> ().color;
				FadeSpeed = (Time.time - TimeActivatedFade) / FadeDuration;
				OriginalFPanelColor.a = Mathf.Lerp (1, 0, FadeSpeed);
				OriginalFTextColor.a = Mathf.Lerp (1, 0, FadeSpeed);
				FadingPromptPanel.GetComponent<Image> ().color = OriginalFPanelColor;
				FadingPromptPanel.GetComponentInChildren<Text> ().color = OriginalFTextColor;
			} else {
				// End of Fading animation, set colors back to their original Alpha 1
				OriginalFPanelColor.a = 1;
				OriginalFTextColor.a = 1;
				FadingPromptPanel.GetComponent<Image> ().color = OriginalFPanelColor;
				FadingPromptPanel.GetComponentInChildren<Text> ().color = OriginalFTextColor;
				FadingPromptPanel.SetActive (false);
				ShowFadingPrompt = false;
			}
		}
	}
}
