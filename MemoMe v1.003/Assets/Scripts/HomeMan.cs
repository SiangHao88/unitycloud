﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;				// For dealing with UI GameObjects
using System.IO;					// For file Operations

// GameState 1: Home / Main Menu
public class HomeMan : MonoBehaviour {

	public GameObject Canvas;				// Referenced in Editor
	public GameObject IndiArtPrefab;		// Referenced to IndiArtPanel prefab in Editor
	public GameObject ExpandingScrollPanel;	// Reference in Editor, IndiArtPanel's parent

	private GameObject DifficultySlider;
	private GameObject DifficultyDisplay;
	public List<GameObject> ArticleList;	// Points to ArticleList in MainEngine

	private int NewGameState;
	public GameObject AddButton;			// Referenced in Editor
	public GameObject SelectedGO;			// A temp buff for any other scripts to update and use
	public int ArticleIndex;				// Article Index in ArticleList that is in focus
	public string SelectedArticleName;		// Holds to article name to facilitate deletion of file (IO operation)

	// Use this for initialization
	void Start () {
		DifficultySlider = Canvas.GetComponent<MainEngine> ().DifficultySlider;
		DifficultyDisplay = Canvas.GetComponent<MainEngine> ().DifficultyDisplay;
		// ArticleList = new List<GameObject>(); 	// Will Override and delete existing data after loaded in OnEnable
		NewGameState = 1;

		ArticleIndex = -1;

		// No need to instantiate GameObjects because C# have default values.
	}

	// Called whenever GameObject is set to Active
	void OnEnable(){
		ArticleList = Canvas.GetComponent<MainEngine> ().ArticleList;
		//LayoutRebuilder.ForceRebuildLayoutImmediate (Canvas.GetComponent<RectTransform> ());
		ClearFullArticleList ();
		//Debug.Log("AfterClear - Article List Count: " + ArticleList.Count);
		Canvas.GetComponent<MainEngine>().ArticleTitle = "";
		UnityEngine.Canvas.ForceUpdateCanvases();
		LoadArticleList();
		//Debug.Log("AfterLoad - Article List Count: " + ArticleList.Count);

	}

	// Called whenever GameObject is set to Active - false
	void OnDisable(){
		ClearFullArticleList ();
	}

	public void ClearFullArticleList(){
		if (ArticleList.Count >= 1) {
			foreach (GameObject GObj in ArticleList) {
				Destroy (GObj);
			}
			ArticleList.Clear ();
		}

	}
		
	public void LoadArticleList(){
		string TempPath = Application.persistentDataPath + "/StudyMaterial";
		// TempPath = "C:/Users/siang/AppData/LocalLow/DefaultCompany/MemoMe/StudyMaterial"

		/* Create The "StudyMaterial" folder if it is has not been created
		 * (Does not do anything if path already exist)
		 * 		~~Especially for 1st time user~~*/
		Directory.CreateDirectory (TempPath);		

		string[] AllFileNames = Directory.GetFiles (TempPath, "*txt");
		foreach (string FullFilePath in AllFileNames) {
			GameObject IndiArtPanelClone = Instantiate (IndiArtPrefab) as GameObject;
			IndiArtPanelClone.GetComponentInChildren<Text> ().text = Path.GetFileNameWithoutExtension (FullFilePath);
			IndiArtPanelClone.transform.SetParent (ExpandingScrollPanel.transform, false);
			//Debug.Log ("IndiArtPanelClone: " + IndiArtPanelClone.name);
			ArticleList.Add (IndiArtPanelClone);
		}
		// Debug.Log ("ArticleList.count: " + ArticleList.Count);
	}

	public void AddButton_Clicked(){						// Public, trigger referenced in Editor
		NewGameState = 2;									// GameState 2: Add New Article
		Canvas.GetComponent<MainEngine>().UpdateGameState(NewGameState);
		//Debug.Log ("Add Button has been clicked :D");
	}

	public void EditButton_Clicked(){
		NewGameState = 4;									// GameState 4: Edit Selected Article

		SelectedArticleName = SelectedGO.transform.GetChild (0).GetChild (0).GetChild (0).GetComponent<Text> ().text;
		Canvas.GetComponent<MainEngine> ().ArticleTitle = SelectedArticleName;
		CheckArticleIndex (SelectedArticleName);

		Canvas.GetComponent<MainEngine>().UpdateGameState(NewGameState);
		/* Things to do:
		 * 1. Set-up interface for Editing Mode
		 * 2. Bring Control To GameState 4: Editing
		 * 3. Create EditMan to take over functionality control*/
	}

	public void DelButton_Clicked(){
		/* Things to do:
		 * 1. Delete The prefab panel
		 * 2. Delete Article from ArticleList
		 * 3. Delete the Actual File
		 * 4. Set Panel selected to None
		 */

		// (IndiArtPanel) ==> IArtScrollPanel ==> DisplayItemButton ==> Text
			
		//SelectedArticleName = SelectedGO.transform.GetChild (0).GetChild (0).GetChild (0).GetComponent<Text> ().text;

		SelectedArticleName = SelectedGO.transform.GetChild (0).GetChild (0).GetChild (0).GetComponent<Text> ().text;
		CheckArticleIndex (SelectedArticleName);
		DeleteArticle (ArticleIndex);

		// Deselecet Selection!!!!

		// Reset Article Selected to null, since last selected article was just deleted.
		//CanvasStateController.ArticleSelected = "";
		//AttachedDisplayButton = this.transform.parent.GetChild (0).GetComponent<GameObject>();
		//Debug.Log ("AttachedDisplayButton.GetComponent<Text>().text: " + AttachedDisplayButton.GetComponent<Text> ().text);
	}

	public void StudyButton_Clicked(){
		NewGameState = 3;									// GameState 3: Study

		if (SelectedGO == null) {
			Canvas.GetComponent<MainEngine> ().ActivateFadingPrompt ("No Article Selected!");
		} else {
			SelectedArticleName = SelectedGO.transform.GetChild (0).GetChild (0).GetChild (0).GetComponent<Text> ().text;
			Canvas.GetComponent<MainEngine> ().ArticleTitle = SelectedArticleName;
			Canvas.GetComponent<MainEngine> ().UpdateGameState (NewGameState);
		}
		// Need to check if there is an article selected
	}

	public void ExamButton_Clicked(){
		NewGameState = 5;
		if (SelectedGO == null) {
			Canvas.GetComponent<MainEngine> ().ActivateFadingPrompt ("No Article Selected!");
		} else {
			SelectedArticleName = SelectedGO.transform.GetChild (0).GetChild (0).GetChild (0).GetComponent<Text> ().text;
			Canvas.GetComponent<MainEngine> ().ArticleTitle = SelectedArticleName;
			Canvas.GetComponent<MainEngine> ().UpdateGameState (NewGameState);
		}
	}

	public void CheckArticleIndex(string ArticleName){
		for (var j = 0; j < ArticleList.Count; j++) {
			// Debug.Log ("Article Name [" + j + "] =" + ArticleList [j].transform.GetChild (0).GetChild (0).GetChild (0).GetComponent<Text> ().text);
			if (ArticleList [j].transform.GetChild (0).GetChild (0).GetChild (0).GetComponent<Text> ().text == SelectedArticleName)
				ArticleIndex = j;
		}
	}

	public void DeleteArticle(int ArticleIndex){
		/* Things to do:
		 * 1. get the article name
		 * 2. Delete code goes here */
		string TempPath = Application.persistentDataPath + "/StudyMaterial";
		// Debug.Log ("Article Index: " + ArticleIndex);
		// Debug.Log ("Article Length: " + ArticleList.Count);
		SelectedArticleName = ArticleList [ArticleIndex].transform.GetChild (0).GetChild (0).GetChild (0).GetComponent<Text> ().text;
		Destroy (ArticleList [ArticleIndex]);
		ArticleList.RemoveAt (ArticleIndex);
		if (File.Exists (TempPath + "/" + SelectedArticleName+".txt")) {
			//Debug.Log ("Selected file <<" + SelectedArticleName + ">> FOUND!!");
			File.Delete(TempPath + "/" + SelectedArticleName+".txt");
		}
		Canvas.GetComponent<MainEngine> ().ArticleTitle = "";
	}

	// Update is called once per frame
	void Update () {
		DifficultyDisplay.GetComponent<Text> ().text = DifficultySlider.GetComponent<Slider> ().value.ToString () + "%";
		
	}
}
