﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;						// For parsing string to int
using System.Linq;					// For displaying content of a list

public class StudyMan : MonoBehaviour {

	public GameObject Canvas;				// Referenced in Editor
	public GameObject HomeState;			// Referenced in Editor, for grabbing of article title
	private string ArticleTitle;
	public GameObject TitleDisplay;			// Referenced in Editor
	public GameObject ContentDisplay;		// Referenced in Editor
	private string ArticleContent;			// For easier handling, instead of always getcomponent
	public GameObject ContentBuffer;		// Referenced in Editor, to help determine how many lines per page

	private int ContentLineCount;			// for easier handling, instead of always GetComponent<Text>().cachedTextGenerator.lineCount		
	private int DisplayedCharCount;			// for easier handling, instead of always GetComponent<Text> ().cachedTextGenerator.characterCountVisible
	private List <string> ContentInPages;	// For processing of Pages
	private List <int> PgCharNumber;		// For processing of Pages
	private int RemainingChars;				// For processing of Pages
	private int CurrentPage;				// To keep track of current page number

	public GameObject PageNumDisplay;		// Referenced in Editor (Page number display)
	public GameObject PrevPageButton;		// Referenced in Editor (for control enable/disable)
	public GameObject NextPageButton;		// Regerenced in Editor (for control enable/disable)
	private Color DefaultBlackTextColor;		// Holds original Color for text as we'll be manipulating Alpha
	private Color DefaultFadedBlackTextColor;	// Manipulate from PrevPageTextColor with Alpha of 0.3

	public GameObject PageNav;				// Referenced in Editor
	public GameObject DifficultyDisplay;	// Referenced in Editor
	private int DifficultyLevel;			// for easier handling, instead of GetComponent

	void OnEnable()
	{
		//ArticleTitle = Canvas.GetComponent<MainEngine>();
		//Debug.Log("CHomeState.GetComponent<HomeMan> ().SelectedGO: " + HomeState.GetComponent<HomeMan> ().SelectedGO);
		if (HomeState.GetComponent<HomeMan> ().SelectedGO != null) {
			
			ArticleTitle = HomeState.GetComponent<HomeMan> ().SelectedGO.GetComponentInChildren<Text>().text;
			//Debug.Log ("Title: " + ArticleTitle);
			TitleDisplay.GetComponent<Text> ().text = ArticleTitle;
			ArticleContent = Canvas.GetComponent<MainEngine> ().LoadContentText (ArticleTitle);
			Canvas.GetComponent<MainEngine> ().ArticleTitle = ArticleTitle;

			//ContentDisplay.GetComponent<Text> ().text = ArticleContent;
			ContentBuffer.GetComponent<Text> ().text = ArticleContent;
			RemainingChars = ArticleContent.Length;	
		}

		PgCharNumber = new List<int>();
		ContentInPages = new List<string> ();			// Actually not required because C# have by default initialized
		CurrentPage = 1;

		DefaultBlackTextColor = TitleDisplay.GetComponentInChildren<Text> ().color;
		DefaultFadedBlackTextColor = new Vector4 (DefaultBlackTextColor.r, DefaultBlackTextColor.g, DefaultBlackTextColor.b, DefaultBlackTextColor.a * 0.3f);
		PageNav.GetComponentInChildren<InputField> ().text = "";

		DifficultyLevel = (int)Canvas.GetComponent<MainEngine> ().DifficultySlider.GetComponent<Slider> ().value;
		DifficultyDisplay.GetComponent<Text> ().text = DifficultyLevel + "%";

		StartCoroutine ("AfterOneFrame");			// We need to get the number of lines only after text has been rendered after accomodating for text wrap etc...
	}

	void OnDisable(){
		//Debug.Log ("here");
		ArticleTitle = "";
		TitleDisplay.GetComponent<Text> ().text = "";
		ArticleContent = "";
		ContentBuffer.GetComponent<Text> ().text = "";
		ContentInPages.Clear ();
		StopAllCoroutines ();
	}


	IEnumerator AfterOneFrame(){
		yield return new WaitForFixedUpdate ();
//		ContentLineCount = ContentBuffer.GetComponent<Text> ().cachedTextGenerator.lineCount;
		DisplayedCharCount = ContentBuffer.GetComponent<Text> ().cachedTextGenerator.characterCountVisible;
//		Debug.Log ("Number of Lines Displayed: " + DisplayedContentLineCount);

//		Debug.Log ("Remaining Chars#: " + RemainingChars + " , Displayed Char Count: " + DisplayedCharCount);
		while (RemainingChars > 0) {
			DisplayedCharCount = ContentBuffer.GetComponent<Text> ().cachedTextGenerator.characterCountVisible;
			PgCharNumber.Add (DisplayedCharCount);
			ContentInPages.Add(ContentBuffer.GetComponent<Text>().text.Substring(0,DisplayedCharCount));
			// Debug.Log ("Page 1 Char count: " + DisplayedCharCount);
			RemainingChars -= DisplayedCharCount;
			ContentBuffer.GetComponent<Text> ().text = ContentBuffer.GetComponent<Text> ().text.Substring (DisplayedCharCount);
			UnityEngine.Canvas.ForceUpdateCanvases ();
//			Debug.Log ("Remaining Chars#: " + RemainingChars + " , Displayed Char Count: " + DisplayedCharCount);
		}

//		Debug.Log ("Total number of pages generated: " + PgCharNumber.Count);
		int TotalCharChecker = 0;
		for (int i = 0; i < PgCharNumber.Count; i++) {
//			Debug.Log ("Page " + i + ": " + PgCharNumber [i]);
			TotalCharChecker += PgCharNumber [i];
		}

		//Debug.Log ("Therefore total number of Chars from all pages: " + TotalCharChecker);
		//Debug.Log ("Versus original Article Text Length: " + ArticleContent.Length);

		ContentDisplay.GetComponent<Text> ().text = ContentInPages [0];
		PageNumDisplay.GetComponent<Text> ().text = "Pg. " + CurrentPage + "/" + ContentInPages.Count;

		// Set button's activate/deactivate state
		UpdateButtonState();

	}

	public void GotoNextPage(){
		//Debug.Log ("Entered Go To Next Page()");
		if (CurrentPage < ContentInPages.Count) {
			CurrentPage++;
			ContentDisplay.GetComponent<Text> ().text = ContentInPages [CurrentPage-1];
			UnityEngine.Canvas.ForceUpdateCanvases ();
		}
		PageNumDisplay.GetComponent<Text> ().text = "Pg. " + CurrentPage + " / " + ContentInPages.Count;
		UpdateButtonState ();
		//Debug.Log ("Current Page#: " + CurrentPage);
		//Debug.Log ("Line Count in Page: " + ContentDisplay.GetComponent<Text> ().cachedTextGenerator.characterCountVisible);
		//Debug.Log ("Number of Lines Visible: " + ContentDisplay.GetComponent<Text> ().cachedTextGenerator.lineCount);
	}

	public void GotoPreviousPage(){
		if (CurrentPage > 1) {
			CurrentPage--;
			ContentDisplay.GetComponent<Text> ().text = ContentInPages [CurrentPage-1];
			UnityEngine.Canvas.ForceUpdateCanvases ();
		}
		PageNumDisplay.GetComponent<Text> ().text = "Pg. " + CurrentPage + "/" + ContentInPages.Count;
		UpdateButtonState ();
		//Debug.Log ("Current Page#: " + CurrentPage);
		//Debug.Log ("Line Count in Page: " + ContentDisplay.GetComponent<Text> ().cachedTextGenerator.characterCountVisible);
		//Debug.Log ("Number of Lines Visible: " + ContentDisplay.GetComponent<Text> ().cachedTextGenerator.lineCount);
	}
	private void UpdateButtonState(){
		// Check for Previous Pgae Button
		if (CurrentPage <= 1) {
			PrevPageButton.GetComponent<Button> ().interactable = false;
			PrevPageButton.GetComponentInChildren<Text> ().color = DefaultFadedBlackTextColor;
		} else {
			PrevPageButton.GetComponent<Button> ().interactable = true;
			PrevPageButton.GetComponentInChildren<Text> ().color = DefaultBlackTextColor;
		}

		// Check for Next Page Button
		if (CurrentPage >= ContentInPages.Count) {
			NextPageButton.GetComponent<Button> ().interactable = false;
			NextPageButton.GetComponentInChildren<Text> ().color = DefaultFadedBlackTextColor;
		} else {
			NextPageButton.GetComponent<Button> ().interactable = true;
			NextPageButton.GetComponentInChildren<Text> ().color = DefaultBlackTextColor;
		}

		// Check for Page Navigator Panel
		if (ContentInPages.Count == 1) {
			/* Disable Page Navigator
			 * 1. Grey out "Page:" text
			 * 2. Disable InputField
			 * 3. Disable "GO" Button */
			PageNav.transform.GetChild (0).GetComponentInChildren<Text> ().color = DefaultFadedBlackTextColor;	// "Page:"
			PageNav.GetComponentInChildren<InputField> ().interactable = false;
			PageNav.GetComponentInChildren<Button> ().interactable = false;
			PageNav.transform.GetChild(2).GetComponentInChildren<Text>().color = DefaultFadedBlackTextColor;	// "GO" button
		} else {
			PageNav.transform.GetChild (0).GetComponentInChildren<Text> ().color = DefaultBlackTextColor;		// "Page:"
			PageNav.GetComponentInChildren<InputField> ().interactable = true;
			PageNav.GetComponentInChildren<Button> ().interactable = true;
			PageNav.transform.GetChild(2).GetComponentInChildren<Text>().color = DefaultBlackTextColor;			// "GO" button
		}
	}
	public void GoButton_Clicked(){
		int PgNumEntered = Int32.Parse(PageNav.GetComponentInChildren<InputField> ().text);
		//Debug.Log ("Page Number Entered: " + PgNumEntered);
		if (PgNumEntered <= 0 || PgNumEntered > ContentInPages.Count) {
			//Debug.Log ("Invalid Page number Input");
			Canvas.GetComponent<MainEngine> ().ActivateFadingPrompt ("Invalid Page Number!");
		} else {
			//Debug.Log ("Jumping to Page#: " + PgNumEntered);
			CurrentPage = PgNumEntered;
			ContentDisplay.GetComponent<Text>().text = ContentInPages[PgNumEntered-1];
			PageNumDisplay.GetComponent<Text> ().text = "Pg. " + CurrentPage + " / " + ContentInPages.Count;
			UpdateButtonState ();
		}
	}

// Not used due to directly use the back button function
/*	public void HomeButton_Clicked(){
 *		Debug.Log ("Home button clicked");
 *	} */

	public void ExamButton_Clicked(){
		int NewGameState = 5;
		Canvas.GetComponent<MainEngine> ().UpdateGameState (NewGameState);
	}

}
