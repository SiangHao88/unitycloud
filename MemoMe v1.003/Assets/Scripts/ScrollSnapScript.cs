﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;			// For Detecting is pointer over gameobject

public class ScrollSnapScript : MonoBehaviour{

	private float SnapActivateDist;			// Horizontal drag distance to activate snap (can be input in Editor)

	private float TargetOriginalXPosition;	// Original x position
	private float CurrentXPosition;			// New x position

	private float DraggedMovement;			// Delta movement: + for dragged right, - for dragged left (cal from TargetOriginalXPosition & CurrentXPosition)
	private bool SnapActive;				// Activates when Delta movement > SnapActivateDIst
	private Vector3 SnapSpeed;				// Speed of Snap Movement

	public bool PanelSelected;				// Vertical (Y-axis) flag to determine which panel is selected (Public for ScrollColorMan Access)

	private float PanelExtensionWidth;		// How munch width the panel has extended out from parent to accomodate Edit & Delete button


	void Start()
	{
		SnapActivateDist = 15;					// Could not by updated in Editor as it is Prefab
		TargetOriginalXPosition = this.transform.localPosition.x;
		CurrentXPosition = 0;
		DraggedMovement = 0;
		SnapActive = false;
		SnapSpeed = new Vector3 (12, 0, 0);		// Could not by updated in Editor as it is Prefab
		PanelSelected = false;

		PanelExtensionWidth = 200;				// Could not be updated in Editor as it is Prefab
	}


	void Update()
	{
		if (Input.GetMouseButtonDown (0)) {

			// Set Original X position of IArtScrollPanel
			//MyGameObject.GetComponent<UnityEngine.UI.ScrollRect>().SetActive(false);​

			TargetOriginalXPosition = this.transform.localPosition.x;

			RectTransform rt = (RectTransform)this.gameObject.transform;
		}
		if (Input.GetMouseButtonUp (0)) {
			//Debug.Log("Current Selected GO: " + EventSystem.current.currentSelectedGameObject.name);
			/* Check if snap to go Left or right
			 * 1. Check displacement (-ve pull left -> Show edit/del button, +ve pull right -> Hide edit/del button)
			 * 2. Check to seeif Activate Snap (displace is > SnapActivateDist)
			 * 3a. If user did not activate Snap, Move panel back to their last position (User accidentally mouse down)
			 * 3b. If user intend to activate snap, Move panel to the new position
			 * 4. Return any extended but not-selected panel back to their original position (i.e. x:0)*/

			// 1. Check if snap to go left or right
			// TargetOriginalXPosition already updated during MouseDown
			CurrentXPosition = this.transform.localPosition.x;
			DraggedMovement = CurrentXPosition - TargetOriginalXPosition;
			// Debug.Log ("Dragged Movement: " + DraggedMovement);
			// Debug.Log ("Dragged Movement (Abs): " + Mathf.Abs(DraggedMovement))	;

			// 2. Check whether to activate snap
			if (Mathf.Abs (DraggedMovement) >= SnapActivateDist) {
				SnapActive = true;
			}
			//Debug.Log ("SnapActive: " + SnapActive + " , *SnapActiveDistance*: " + SnapActivateDist + " , DraggedMovement (Abs):" + Mathf.Abs(DraggedMovement));
		}

		// 3a. Move Selected Panel (Case where user accidentally moved, i.e. SnapActive = false)
		if (PanelSelected & !SnapActive) {
			if (DraggedMovement < 0) {					// At x:0,  User Accidentally pulled slight left
				if (this.transform.localPosition.x < 0 && this.transform.localPosition.x >= -SnapActivateDist) {
					/* 1st check: Only move when it haven't return to original position (i.e. 0 )
					 * 2nd check: Prevent Snap when panel is full extended to left 
					 * 		(i.e. if x extends over SnapActivateDistance, panel was purposely extended all 
					 * 		the way to the left)*/
					//Debug.Log ("Entered DraggedMovement < 0");
					transform.position += SnapSpeed;	// Push it Right, back to original position
				}
			} else if (DraggedMovement > 0) {			// At x:-200, User Accidentally pull slight right
				if (this.transform.localPosition.x > -PanelExtensionWidth && this.transform.localPosition.x <= -SnapActivateDist) {
					/* 1st check: Only move when it haven't return to original position (i.e. -200 )
					 * 2nd check: Prevent Snap when panel is full extended to right 
					 * 		(i.e. if x extends over SnapActivateDistance, panel was purposely extended all 
					 * 		the way to the right -> default 0 position)*/
					//Debug.Log ("Here (DraggedMovement > 0 && SnapActive = true)");
					//Debug.Log("Entered DraggedMovement > 0");
					transform.position -= SnapSpeed;	// Push it left, back to extended left position
				}
			}
		}

		// 3b. Move Selected Panel (Case where user purposefully move a panel, i.e. SnapActive = true)
		//Debug.Log("PanelSelected: " + PanelSelected +" , SnapActive: " + SnapActive + " (Should be both TRUE)");
		if (PanelSelected && SnapActive) {
			//Debug.Log ("DraggedMovement: " + DraggedMovement + " , (should be < 0)");
			if (DraggedMovement < 0) {							// -ve Drag, user pull left
				//Debug.Log("LocalPosX: "+ this.transform.localPosition.x + " > -PanelExtensionWidth: " + -PanelExtensionWidth);
				if (this.transform.localPosition.x > -PanelExtensionWidth) {	// Only move when Panel haven't hit edge (left - 200)
					//Debug.Log ("here " + this.transform.localPosition.x);
					transform.position -= SnapSpeed;
				} else {										// Once Panel reach destination, reset flags
					SnapActive = false;						
					//PanelSelected = false;
				}
			} else if (DraggedMovement > 0) {					// +ve Drag, user pull right
				if (this.transform.localPosition.x < 0) {		// Only move when Panel haven't hit edge (left, i.e. 0)
					transform.position += SnapSpeed;
				} else {										// Once Panel reach destination, reset flags
					SnapActive = false;
					//PanelSelected = false;
				}
			}
		}

		// 4. Return Non-selected panels to their default x:0 position
		if (!PanelSelected) {
			if (this.transform.localPosition.x < 0) { 			// Check whether it is to extended to left
				transform.position += SnapSpeed;				// Move panel back to right, until it hit 0 and auto snap back by unity
			}
		}
			
	}
}
