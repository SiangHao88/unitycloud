﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResizeIField : MonoBehaviour {

	public GameObject BufferText;				// Referenced in Editor
	public GameObject Canvas;					// Referenced in Editor, for Fading Prompt
	private float OriginalContentInputHeight;	// Original height is defined by anchors, which solves the problem of different screen resolution when used in sizeDelta.

	void OnEnable(){
		OriginalContentInputHeight = this.GetComponent<RectTransform> ().rect.height;
	}

	// Update is called once per frame
	void Update () {
		if (Input.anyKeyDown) {
			ResizeIF();
		}
	}//

	private void ResizeIF(){
		//Debug.Log ("CharCount IF: " + this.GetComponent<InputField> ().text.Length);
		if (this.GetComponent<InputField> ().text.Length >= 16000) {
			Canvas.GetComponent<MainEngine> ().ActivateFadingPrompt ("Input exceeded maximum of 16k characters!!\nInput will be truncated!!");
		}
		BufferText.GetComponent<Text> ().text = this.GetComponent<InputField> ().text;

		// Force Contentsize fitter to act if user suddenly delete all content ("select all -> delete");
		if (BufferText.GetComponent<Text> ().text == "") {
			BufferText.GetComponent<Text> ().text = "A";
			BufferText.GetComponent<Text> ().text = "";
		}

		// Because BufferText have ContentSizeFitter, it will auto resize according to In(
		if (BufferText.GetComponent<RectTransform>().rect.height > this.GetComponent<RectTransform>().rect.height ){
			this.GetComponent<RectTransform> ().sizeDelta = new Vector2 (0, BufferText.GetComponent<RectTransform>().rect.height - OriginalContentInputHeight);
			// DeltaX = -150 because got -75 on both left and right
		}
	}
}
