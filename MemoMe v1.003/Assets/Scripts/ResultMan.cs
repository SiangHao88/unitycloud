﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;	
using UnityEngine.Advertisements;

public class ResultMan : MonoBehaviour {

	public GameObject Canvas;				// Referenced in Editor
	private string ArticleTitle;
	public GameObject TitleDisplay;			// Referenced in Editor
	private int DifficultyLevel;
	public GameObject DifficultyLevelDisplay;
	public GameObject ScoreDisplay;			// Referenced in Editor

	public GameObject ExamState;			// Referenced in Editor
	private List <string> AnswerInPages;	// Reference from ExamMan, for score tabulation
	private List <string> HintInPages;		// Reference from ExamMan, for score tabulation
	private List <string> ContentInPages;	// Reference from ExamMan, for score tabulation
	private List <int> OriginalBlankoArr;	// Keep position references of "_" from Original Content Stripped
	private List <int> HintBlankoArr;		// Keep position references of "_" from Hint Stripped
	//private List <int> AnswerBlankoArr;		// Keep position references of "_" from Answer Stripped
	private string ContentBuffer;			// Picks up Correct Letters from Original Content with help of HintBlankoArr
	private string AnswerBuffer;			// Picks up Answer input from AnswerBlankoArr with help of HintBlankoArr
	private int ScoreCounter;				// Counter for the number of correct letters
	private float FinalScore;					// To be tabulated from ScoreCounter with the number of tested letters

	private string AnswerStripped;	// For stripping of 'spaces' (avoid space messing with text position)
	private string HintStripped;	// For stripping of 'spaces' (avoid space messing with text position)	
	private string ContentStripped;	// For stripping of 'spaces' (avoid space messing with text position)

	// Trying to use start() because of XX% issue aft fresh installation
	void Start(){
		Debug.Log ("Start from ResultMan Testing Latest Transfer");
		TabulateScore ();
		ShowAds ();
	}

	void OnEnable () {
		Debug.Log ("OnEnable from ResultMan");
		ArticleTitle = Canvas.GetComponent<MainEngine> ().ArticleTitle;
		TitleDisplay.GetComponent<Text> ().text = Canvas.GetComponent<MainEngine> ().ArticleTitle;;
		DifficultyLevel = (int)Canvas.GetComponent<MainEngine> ().DifficultySlider.GetComponent<Slider> ().value;
		DifficultyLevelDisplay.GetComponent<Text> ().text = "" + DifficultyLevel + "%";

		AnswerInPages = ExamState.GetComponent<ExamMan> ().AnswerInPages;
		HintInPages = ExamState.GetComponent<ExamMan> ().HintInPages;
		ContentInPages = ExamState.GetComponent<ExamMan> ().ContentInPages;
		ContentBuffer = "";
		AnswerBuffer = "";
		ScoreCounter = 0;
		FinalScore = 0;


		//TabulateScore ();
		//ShowAds ();
	}

	public void TabulateScore(){
		/* Things to do:
		 * 1. Remove white space on original text
		 * 2. Get Position of Original '_'
		 * 3. Remove white space on HINT
		 * 4. Get Position of '_' on HINT
		 * 5. Filter out '_' from HINT version from original '_'
		 * 6. Grab Letters from Original Content on positions from (5)
		 * 7. Grab Letters from Answers on positions from (5)
		 * 8. Compare (6) with (7)
		 * 9. Tabulate the score */

		// 1. Remove white space on original text
		ContentStripped = RemoveWhiteSpace(ContentInPages);
		HintStripped = RemoveWhiteSpace (HintInPages);
		AnswerStripped = RemoveWhiteSpace(AnswerInPages);
		// Debug.Log ("Answer without space: " + AnswerStripped);

		// 2. Get Position of "_" in Original Stripped
		OriginalBlankoArr = GetBlankoPos(ContentStripped);
		HintBlankoArr = GetBlankoPos (HintStripped);

		// ... 5. Filter out '_' from Hint version from Original
		for (int i = 0; i < OriginalBlankoArr.Count; i++) {
			for (int j = 0; j < HintBlankoArr.Count; j++){
				if (HintBlankoArr [j] == OriginalBlankoArr [i]) {
					HintBlankoArr.RemoveAt (j);
				}
			}
		}

		// 6. Grab letters from Original Content on the tested spots
		for (int i = 0; i < HintBlankoArr.Count; i++) {
			ContentBuffer += ContentStripped [HintBlankoArr [i]];
		}
		//Debug.Log ("Tested letters: " + ContentBuffer);

		// We need to ensure that Answer is complete, else we'll be pulling for Answer in undefined indexes
		Debug.Log("Max Value in position array (which index is the last answer spot?):" + HintBlankoArr.Max());
		// In case user didn't complete the test
		if (AnswerStripped.Length < HintBlankoArr.Max ()) {
			ScoreDisplay.GetComponent<Text> ().text = "Incomplete!";
		} else {

			// 7. Grab letters from AnswerStripped on the tested spots
			for (int i = 0; i < HintBlankoArr.Count; i++) {
				AnswerBuffer += AnswerStripped [HintBlankoArr [i]];
			}
			//Debug.Log ("Tested Input: " + AnswerBuffer);

			// 8. Compare ContentBuffer with AnswerBuffer (convert both to lowercase for easy comparison)
			ContentBuffer = ContentBuffer.ToLower ();
			AnswerBuffer = AnswerBuffer.ToLower ();
			for (int i = 0; i < ContentBuffer.Length; i++) {
				if (ContentBuffer [i] == AnswerBuffer [i]) {
					ScoreCounter++;
				}
			}
			//Debug.Log ("ScoreCounter: " + ScoreCounter);

			Debug.Log ("ContentBuffer.length =" + ContentBuffer.Length);
			float NumOfTestedLetters = (float)ContentBuffer.Length;

			// 9. Tabulate the score
			if (ContentBuffer.Length > 0) {		// Avoid any division by 0 error
				//Debug.Log("HERE!");
				FinalScore = (float)ScoreCounter * 100f / (float)ContentBuffer.Length;
			}

			// Update the score =D
			Debug.Log ("Final Score = " + FinalScore);
			ScoreDisplay.GetComponent<Text> ().text = FinalScore.ToString ("0.00") + "%";


			// Check if Blanko Arrays are correct
			/*if (HintBlankoArr.Count > 0) {
			for (int i = 0; i < HintBlankoArr.Count; i++) {
				Debug.Log ("Tested Blanko[" + i + "]: " + HintBlankoArr [i]);
			}
		}*/
		}	
	}

	public List<int> GetBlankoPos(string TextWithBlanko){
		List<int> BlankoPos = new List<int>();
		for (int i = 0 ; i < TextWithBlanko.Length ; i++){
			if (TextWithBlanko [i] == '_') {
				BlankoPos.Add (i);
			}
		}

		return BlankoPos;
	}

	public string RemoveWhiteSpace(List <string> TextToRemove){
		string FinalNoSpace = "";
		// Debug.Log ("TextToRemove.Count: "+ TextToRemove.Count);
		// Debug.Log ("Count of AnswerInPages:" + TextToRemove.Count);
		for (int i = 0; i < TextToRemove.Count; i++) {		// Joint all pages to 1 string
			FinalNoSpace += TextToRemove [i];
		}

		FinalNoSpace = FinalNoSpace.Replace (" ", "");		// Remove space
		FinalNoSpace = FinalNoSpace.Replace ("\n", "");		// Remove New Line
		return FinalNoSpace;
	}

	public void ReviewButton_Clicked(){
		//Debug.Log ("Article Title = " + ArticleTitle);
		//Debug.Log("Count of Answer In Pages: " + AnswerInPages.Count);
		//Debug.Log("Count of Hint In Pages: " + HintInPages.Count);
		//Debug.Log("Count of Content In Pages: " + ContentInPages.Count);
	}

	public void HomeButton_Clicked(){
		//Canvas.GetComponent<MainEngine> ().OldState = true;			// Disable updating of state history (no not save new state)

		ResultCleanUp();		// Clean up all variables to prevent stacking on subsequent entry

		Canvas.GetComponent<MainEngine> ().StateHistory.Clear ();	// Blast clear State History
		Canvas.GetComponent<MainEngine> ().UpdateGameState (1);		// 1 to home state
	}

	public void ShowAds (){
		if (Advertisement.IsReady ()) {
			Advertisement.Show ();
			// Advertisement.Show ("video", new ShowOptions (){ resultCallback = HandleAdsResult });
			// Note: "Video" is Placement ID found in Dashboard --> Monetize --> Platforms --> Google Play Store
			//		 resultCallBack is to call HandleAdsResult function
		}
	}

	public void ResultCleanUp()
	{
		if (AnswerInPages.Count > 0)	AnswerInPages.Clear ();
		if (HintInPages.Count > 0)		HintInPages.Clear ();
		if (ContentInPages.Count > 0)	ContentInPages.Clear ();

		if (OriginalBlankoArr.Count > 0)	OriginalBlankoArr.Clear ();
		if (HintBlankoArr.Count > 0)		HintBlankoArr.Clear ();
		// if (AnswerBlankoArr.Count > 0)		AnswerBlankoArr.Clear ();	// Not used

		ContentBuffer = "";
		AnswerBuffer = "";
		ScoreCounter = 0;
		FinalScore = 0.0f;

		AnswerStripped = "";
		HintStripped = "";
		ContentStripped = "";
	}

	private void HandleAdsResult(ShowResult result){
		Debug.Log ("Hello");
		switch (result) {
		case ShowResult.Finished:
			Debug.Log ("Player gained +5 Gems!");
			break;
		case ShowResult.Skipped:
			Debug.Log ("Player didn't fully watch the Ads");
			break;
		case ShowResult.Failed:
			Debug.Log ("Player failed to Launch Ads - Check Your internet connection!");
			break;
		}
	}
}
