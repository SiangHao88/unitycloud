﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KBEx : MonoBehaviour {

    public GameObject MyInputField;     //Linking it in Editor

	// Use this for initialization
	void Start () {
		// Window.SetSoftInputMode (SoftInput.StateAlwaysHidden);

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void UpdateText(GameObject ChoosenKey)
    {
        //Letter Choosen: ChoosenKey.name
        if (ChoosenKey.name == "SpaceBar")
        {
            MyInputField.GetComponent<InputField>().text += " ";
        }
        else if (ChoosenKey.name == "BackSpace")
        {
            MyInputField.GetComponent<InputField>().text = MyInputField.GetComponent<InputField>().text.Remove(MyInputField.GetComponent<InputField>().text.Length - 1);
        }
        else if (ChoosenKey.name == "Return")
        {
            MyInputField.GetComponent<InputField>().text += '\n';
        }
        else { 
            MyInputField.GetComponent<InputField>().text += ChoosenKey.name;
        }
    }
}
